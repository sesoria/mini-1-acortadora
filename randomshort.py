import webapp
import shelve
from random import randint

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Url a acortar: </label>
        <input type="text" name="url" required>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

resources = shelve.open("resources")

class Randomshort(webapp.Webapp):
    global resources

    def parse(self, received):
        
        received = received.decode()
        method = received.split(" ")[0]  # me quedo con el protocolo
        resource = received.split(" ")[1]  # me quedo con la peticion del cliente
        body = received.split("\r\n\r\n")[1]  # me quedo con el body

        return method, resource, body  # Se envia en forma de tupla

    def show_resources(self):
        
        msg = ""
        for key, value in resources.items():
            msg += f'URl: {key}, New URL: {value}\n'
        return msg

    def get_original_url(self, resource):
        
        for key, value in resources.items():
            if value == resource:
                return key

    def url_in_dict(self, url):
        
        is_in = False
        list_urls = list(resources.keys())
        for u in list_urls:
            if url == u:
                is_in = True
        return is_in

    def clear_url(self, url):
        
        url = url.replace("%3A", ":")
        url = url.replace("%2F%2F", "//")
        url = url.replace("%2F", "/")
        url = url.replace("+","")
        
        matches = ["http://","https://"]
        if not any([x in url for x in matches]):
            url = "".join(("https://", url))
        return url

    def do_post(self, body):
        
        url = body.split("=")[1]
        url = self.clear_url(url)
        if not self.url_in_dict(url):
            short_url = randint(1, 10000)
            short_url = "".join((f"http://localhost:{self.port}/", str(short_url)))
            resources[url] = short_url

        http = "200 OK"
        html = f"<html><h1>Bienvenido a la recortadora de Urls</h1>" \
               "<body>" \
               f"<div>{FORM}</div>" \
               f"<div>Urls acortadas: {self.show_resources()}</div>" \
               "</body></html>"
        return http, html

    def do_get(self, resource):
        
        aux_resource = "".join((f"http://localhost:{self.port}", resource))
        if aux_resource in resources.values():
            http = '301 Moved Permanently'
            html = f'<html><head><meta http-equiv="refresh" content = "5;url={self.get_original_url(aux_resource)}"</head>' \
                    + f'<html><body><h1>Usted viene del recurso {aux_resource} y ser&aacute;' \
                    + f' redireccionado en 5 segundos a {self.get_original_url(aux_resource)}</h1>' \
                    + '</body></html>'
        elif resource == "/":
            http = "200 OK"
            html = f"<html><h1>Bienvenido a la recortadora de Urls</h1>" \
                    "<body>" \
                    f"<div>{FORM}</div>" \
                    f"<div>Urls acortadas: {self.show_resources()}</div>" \
                    "</body></html>"
        else:
            http = '200 OK'
            html = "<html><body><h1>" + \
                    f"HTTP ERROR: recurso {resource} no disponible." \
                    "</h1>"
        return http, html
    
    def process(self, analyzed):
        method, resource, body = analyzed  # Se desempaqueta la tupla

        if method == "POST":
            http, html = self.do_post(body)

        if method == "GET":
            http, html = self.do_get(resource)

        return http, html

if __name__ == "__main__":
    random_short = Randomshort("localhost", 1234)